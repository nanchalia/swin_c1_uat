/**
 * ServerErrorFault.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package com.rightnow.ws.wsdl;

public class ServerErrorFault extends java.lang.Exception {
    private static final long serialVersionUID = 1507028702856L;
    private com.rightnow.ws.faults.ServerErrorFault faultMessage;

    public ServerErrorFault() {
        super("ServerErrorFault");
    }

    public ServerErrorFault(java.lang.String s) {
        super(s);
    }

    public ServerErrorFault(java.lang.String s, java.lang.Throwable ex) {
        super(s, ex);
    }

    public ServerErrorFault(java.lang.Throwable cause) {
        super(cause);
    }

    public void setFaultMessage(com.rightnow.ws.faults.ServerErrorFault msg) {
        faultMessage = msg;
    }

    public com.rightnow.ws.faults.ServerErrorFault getFaultMessage() {
        return faultMessage;
    }
}
