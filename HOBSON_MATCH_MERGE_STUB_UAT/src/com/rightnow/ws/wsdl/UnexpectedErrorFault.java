/**
 * UnexpectedErrorFault.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package com.rightnow.ws.wsdl;

public class UnexpectedErrorFault extends java.lang.Exception {
    private static final long serialVersionUID = 1507028702849L;
    private com.rightnow.ws.faults.UnexpectedErrorFault faultMessage;

    public UnexpectedErrorFault() {
        super("UnexpectedErrorFault");
    }

    public UnexpectedErrorFault(java.lang.String s) {
        super(s);
    }

    public UnexpectedErrorFault(java.lang.String s, java.lang.Throwable ex) {
        super(s, ex);
    }

    public UnexpectedErrorFault(java.lang.Throwable cause) {
        super(cause);
    }

    public void setFaultMessage(com.rightnow.ws.faults.UnexpectedErrorFault msg) {
        faultMessage = msg;
    }

    public com.rightnow.ws.faults.UnexpectedErrorFault getFaultMessage() {
        return faultMessage;
    }
}
