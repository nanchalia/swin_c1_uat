
public class Record {
	private String Hobsons_CRM_ID;
	private String FirstName;
	private String LastName;
	private String Email;
	private String Email2;
	private String DOB;
	private String Subscription_Email;
	private String Subscription_Phone;
	private String Subscription_SMS;
	private String International;
	private String CountryOfResidence;
	private String CorrespondenceAddressPostcode;
	private String CorrespondenceMobile;
	private String Course;
	private String CourseAreaOfStudy;
	private String BestDescribesYou;
	private String HighestQualification;
	private String ContactChannel;
	/**
	 * @param hobsons_CRM_ID
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param email2
	 * @param dOB
	 * @param subscription_Email
	 * @param subscription_Phone
	 * @param subscription_SMS
	 * @param international
	 * @param countryOfResidence
	 * @param correspondenceAddressPostcode
	 * @param correspondenceMobile
	 * @param course
	 * @param courseAreaOfStudy
	 * @param bestDescribesYou
	 * @param highestQualification
	 * @param contactChannel
	 */
	public Record(){
		
	}
	public Record(String hobsons_CRM_ID, String firstName, String lastName, String email, String email2, String dOB,
			String subscription_Email, String subscription_Phone, String subscription_SMS, String international,
			String countryOfResidence, String correspondenceAddressPostcode, String correspondenceMobile, String course,
			String courseAreaOfStudy, String bestDescribesYou, String highestQualification, String contactChannel) {
		super();
		Hobsons_CRM_ID = hobsons_CRM_ID;
		FirstName = firstName;
		LastName = lastName;
		Email = email;
		Email2 = email2;
		DOB = dOB;
		Subscription_Email = subscription_Email;
		Subscription_Phone = subscription_Phone;
		Subscription_SMS = subscription_SMS;
		International = international;
		CountryOfResidence = countryOfResidence;
		CorrespondenceAddressPostcode = correspondenceAddressPostcode;
		CorrespondenceMobile = correspondenceMobile;
		Course = course;
		CourseAreaOfStudy = courseAreaOfStudy;
		BestDescribesYou = bestDescribesYou;
		HighestQualification = highestQualification;
		ContactChannel = contactChannel;
	}
	/**
	 * @return the hobsons_CRM_ID
	 */
	public String getHobsons_CRM_ID() {
		return Hobsons_CRM_ID;
	}
	/**
	 * @param hobsons_CRM_ID the hobsons_CRM_ID to set
	 */
	public void setHobsons_CRM_ID(String hobsons_CRM_ID) {
		Hobsons_CRM_ID = hobsons_CRM_ID;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return FirstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return LastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return Email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		Email = email;
	}
	/**
	 * @return the email2
	 */
	public String getEmail2() {
		return Email2;
	}
	/**
	 * @param email2 the email2 to set
	 */
	public void setEmail2(String email2) {
		Email2 = email2;
	}
	/**
	 * @return the dOB
	 */
	public String getDOB() {
		return DOB;
	}
	/**
	 * @param dOB the dOB to set
	 */
	public void setDOB(String dOB) {
		DOB = dOB;
	}
	/**
	 * @return the subscription_Email
	 */
	public String getSubscription_Email() {
		return Subscription_Email;
	}
	/**
	 * @param subscription_Email the subscription_Email to set
	 */
	public void setSubscription_Email(String subscription_Email) {
		Subscription_Email = subscription_Email;
	}
	/**
	 * @return the subscription_Phone
	 */
	public String getSubscription_Phone() {
		return Subscription_Phone;
	}
	/**
	 * @param subscription_Phone the subscription_Phone to set
	 */
	public void setSubscription_Phone(String subscription_Phone) {
		Subscription_Phone = subscription_Phone;
	}
	/**
	 * @return the subscription_SMS
	 */
	public String getSubscription_SMS() {
		return Subscription_SMS;
	}
	/**
	 * @param subscription_SMS the subscription_SMS to set
	 */
	public void setSubscription_SMS(String subscription_SMS) {
		Subscription_SMS = subscription_SMS;
	}
	/**
	 * @return the international
	 */
	public String getInternational() {
		return International;
	}
	/**
	 * @param international the international to set
	 */
	public void setInternational(String international) {
		International = international;
	}
	/**
	 * @return the countryOfResidence
	 */
	public String getCountryOfResidence() {
		return CountryOfResidence;
	}
	/**
	 * @param countryOfResidence the countryOfResidence to set
	 */
	public void setCountryOfResidence(String countryOfResidence) {
		CountryOfResidence = countryOfResidence;
	}
	/**
	 * @return the correspondenceAddressPostcode
	 */
	public String getCorrespondenceAddressPostcode() {
		return CorrespondenceAddressPostcode;
	}
	/**
	 * @param correspondenceAddressPostcode the correspondenceAddressPostcode to set
	 */
	public void setCorrespondenceAddressPostcode(String correspondenceAddressPostcode) {
		CorrespondenceAddressPostcode = correspondenceAddressPostcode;
	}
	/**
	 * @return the correspondenceMobile
	 */
	public String getCorrespondenceMobile() {
		return CorrespondenceMobile;
	}
	/**
	 * @param correspondenceMobile the correspondenceMobile to set
	 */
	public void setCorrespondenceMobile(String correspondenceMobile) {
		CorrespondenceMobile = correspondenceMobile;
	}
	/**
	 * @return the course
	 */
	public String getCourse() {
		return Course;
	}
	/**
	 * @param course the course to set
	 */
	public void setCourse(String course) {
		Course = course;
	}
	/**
	 * @return the courseAreaOfStudy
	 */
	public String getCourseAreaOfStudy() {
		return CourseAreaOfStudy;
	}
	/**
	 * @param courseAreaOfStudy the courseAreaOfStudy to set
	 */
	public void setCourseAreaOfStudy(String courseAreaOfStudy) {
		CourseAreaOfStudy = courseAreaOfStudy;
	}
	/**
	 * @return the bestDescribesYou
	 */
	public String getBestDescribesYou() {
		return BestDescribesYou;
	}
	/**
	 * @param bestDescribesYou the bestDescribesYou to set
	 */
	public void setBestDescribesYou(String bestDescribesYou) {
		BestDescribesYou = bestDescribesYou;
	}
	/**
	 * @return the highestQualification
	 */
	public String getHighestQualification() {
		return HighestQualification;
	}
	/**
	 * @param highestQualification the highestQualification to set
	 */
	public void setHighestQualification(String highestQualification) {
		HighestQualification = highestQualification;
	}
	/**
	 * @return the contactChannel
	 */
	public String getContactChannel() {
		return ContactChannel;
	}
	/**
	 * @param contactChannel the contactChannel to set
	 */
	public void setContactChannel(String contactChannel) {
		ContactChannel = contactChannel;
	}
	
	
}
