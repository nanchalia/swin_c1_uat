import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;

import com.rightnow.ws.wsdl.RequestErrorFaultDetail;

public class RecordCollector {
	private ArrayList<Record> recordCollector;
	private ArrayList<Lookup> menuLookup;
	
	//private ArrayList<Lookup> countryLookup;
	//private ArrayList<Lookup> highestQualificationLookup;
	//private ArrayList<Lookup> areaOfStudyLookup;
	//private ArrayList<Lookup> bestDescribesLookup;
	//private ArrayList<Lookup> contactChannelLookup;
	final static Logger log = Logger.getLogger("Record Collector");
	
	public RecordCollector(){
		recordCollector = new ArrayList<Record>();
		menuLookup = new ArrayList<Lookup>();
				
	}
	//GET COUNT
	public int getRecordCount(){
		return recordCollector.size();
	}
	
	public int getLookupCount(){
		return menuLookup.size();
	}
	
	
	//Get DATA
	public Record getRecord(int index){
		return recordCollector.get(index);
	}
	
	public Lookup getMenuLookup(int id){
		return menuLookup.get(id);
	}
	public String lookupValue(long id){
		String value = null;
		Lookup temp;
		for(int i=0;i<this.getLookupCount();i++){
			temp = this.getMenuLookup(i);
			if(temp.getId() == id){
				value = temp.getValue();
			}
		}
		return value;
	}
	//Add DATA	
	public void addRecord(Record record){
		this.recordCollector.add(record);
		
	}
	
	public void addValues(Lookup newValue){
		this.menuLookup.add(newValue);
	}
	
	public boolean readInputFile(String InputFilePath){
		log.info("Reading input file...");
		try{
			Scanner scanner = new Scanner(new FileReader(InputFilePath));
			String line;
			Record inputFileRecord;
			
			//skip header
			scanner.nextLine();
			while(scanner.hasNextLine()){
				line = scanner.nextLine();
				String[] result = line.split(",", -1);
				
				String Hobsons_CRM_ID = result[0];
				String DateCreated = result[1];
				String FirstName = result[2];
				String LastName = result[3];
				String Email = result[4];
				String Email2 = result[5];
				String DateOfBirth = result[6];
				String Subscription_Email = result[7];
				String Subscription_Phone = result[8];
				String Subscription_SMS = result[9];
				String International = result[10];
				String CountryOfResidence = result[11];
				String CorrespondenceMobile = result[13];
				String CorrespondenceAddressPostCode = result[12];
				String Course = result[14];
				String CourseAreaOfStudy = result[15];
				String BD = result[16];
				String HQ = result[17];
				String ContactChannel = result[18];
				
				inputFileRecord = new Record(
										Hobsons_CRM_ID,
										FirstName,
										LastName,
										Email,
										Email2,
										DateOfBirth,
										Subscription_Email,
										Subscription_Phone,
										Subscription_SMS,
										International,
										CountryOfResidence,
										CorrespondenceAddressPostCode,
										CorrespondenceMobile,
										Course,
										CourseAreaOfStudy,
										BD,
										HQ,
										ContactChannel						
										
									);
				recordCollector.add(inputFileRecord);
				
				
			}
			scanner.close();
		}
		catch(Exception e)
		{
			log.error("File Read Error" + e.toString());
			return false;
		}
		
		return true;
		
	}
	
	public boolean readLookupFile(String path){
		//log.info("Preparing lookup file...");
		try{
			Scanner scanner = new Scanner(new FileReader(path));
			String line;
			Lookup lookupValues;
			
			//skip header
			scanner.nextLine();
			while(scanner.hasNextLine()){
				line = scanner.nextLine();
				String[] result = line.split(",");
				lookupValues = new Lookup(Integer.parseInt(result[0]), result[1]);
				menuLookup.add(lookupValues);
				
				
			}
			scanner.close();
		}
		catch(Exception e)
		{
			log.error("File Read Error" + e.toString());
			return false;
		}
		
		return true;
	}
	
	public void printRecord(int index){
		Record r = this.getRecord(index);
		System.out.println("Hobsons CRM ID: " + r.getHobsons_CRM_ID());
		System.out.println("Date Created: ");
		System.out.println("First Name: " + r.getFirstName());
		System.out.println("Last Name: " + r.getLastName());
		System.out.println("Email: " + r.getEmail());
		System.out.println("Email2: " + r.getEmail2());
		System.out.println("Data of Birth: " + r.getDOB());
		System.out.println("Subscribed_Email: " + r.getSubscription_Email());
		System.out.println("Subscribed_Phone: " + r.getSubscription_Phone());
		System.out.println("Subscribed_SMS: " + r.getSubscription_SMS());
		System.out.println("International: " + r.getInternational());
		System.out.println("Country of Residence: " + r.getCountryOfResidence());
		System.out.println("CorrespondenceMobile: " + r.getCorrespondenceMobile());
		System.out.println("CorrespondeceAddressPostCode: " + r.getCorrespondenceAddressPostcode());
		System.out.println("Course: " + r.getCourse());
		System.out.println("Area of Study: " + r.getCourseAreaOfStudy());
		System.out.println("Best Describes: " + r.getBestDescribesYou());
		System.out.println("Highest Qualification: " + r.getHighestQualification());
		System.out.println("Contact Channel: " + r.getContactChannel());
		
	}
	
	public void printLookupValues(int index){
		Lookup l = this.getMenuLookup(index);
		System.out.println(l.getId()+"/"+ l.getValue());
	}
	
	@SuppressWarnings("null")
	public Record compareRecords(int index, Record c1Record) throws RequestErrorFaultDetail, AxisFault{
		Record r = this.getRecord(index);
		log.info("Comparing record");
		//Record c1Record;
		Record temp = new Record();
		//RNWebservice rnWebService = new RNWebservice();
		//c1Record = rnWebService.RNCheckRecord(r);
		if(c1Record != null){
			//System.out.println("Comparing Records");
			//System.out.println(c1Record.getFirstName().equals("null"));
			temp.setHobsons_CRM_ID(r.getHobsons_CRM_ID());
			temp.setFirstName((c1Record.getFirstName()!=null)? c1Record.getFirstName(): r.getFirstName());
			temp.setLastName((c1Record.getLastName()!=null)? c1Record.getLastName(): r.getLastName());
			temp.setEmail(r.getEmail());
			temp.setEmail2(r.getEmail2());
			temp.setDOB((c1Record.getDOB()!= null)? c1Record.getDOB() : r.getDOB());
			temp.setSubscription_Email(r.getSubscription_Email());
			temp.setSubscription_Phone(r.getSubscription_Phone());
			temp.setSubscription_SMS(r.getSubscription_SMS());
			temp.setInternational((c1Record.getInternational() != null)? c1Record.getInternational(): r.getInternational());
			temp.setCountryOfResidence((c1Record.getCountryOfResidence() != null)? c1Record.getCountryOfResidence() : r.getCountryOfResidence());
			temp.setCorrespondenceAddressPostcode((c1Record.getCorrespondenceAddressPostcode()!= null)? c1Record.getCorrespondenceAddressPostcode() : r.getCorrespondenceAddressPostcode());
			temp.setCorrespondenceMobile((c1Record.getCorrespondenceMobile()!= null) ? c1Record.getCorrespondenceMobile() : r.getCorrespondenceMobile());
			temp.setCourse(r.getCourse());
			temp.setCourseAreaOfStudy((c1Record.getCourseAreaOfStudy() != null)? c1Record.getCourseAreaOfStudy():r.getCourseAreaOfStudy());
			temp.setBestDescribesYou((c1Record.getBestDescribesYou() != null)? c1Record.getBestDescribesYou(): r.getBestDescribesYou());
			temp.setHighestQualification((c1Record.getHighestQualification() != null)? c1Record.getHighestQualification() : r.getHighestQualification());
			temp.setContactChannel(r.getContactChannel());
			
		}
		else{
			temp.setHobsons_CRM_ID(r.getHobsons_CRM_ID());
			temp.setFirstName(r.getFirstName());
			temp.setLastName(r.getLastName());
			temp.setEmail(r.getEmail());
			temp.setEmail2(r.getEmail2());
			temp.setDOB(r.getDOB());
			temp.setSubscription_Email(r.getSubscription_Email());
			temp.setSubscription_Phone(r.getSubscription_Phone());
			temp.setSubscription_SMS(r.getSubscription_SMS());
			temp.setInternational(r.getInternational());
			temp.setCountryOfResidence(r.getCountryOfResidence());
			temp.setCorrespondenceAddressPostcode(r.getCorrespondenceAddressPostcode());
			temp.setCorrespondenceMobile(r.getCorrespondenceMobile());
			temp.setCourse(r.getCourse());
			temp.setCourseAreaOfStudy(r.getCourseAreaOfStudy());
			temp.setBestDescribesYou(r.getBestDescribesYou());
			temp.setHighestQualification(r.getHighestQualification());
			temp.setContactChannel(r.getContactChannel());
		}
		return temp;
	}
	
	public void createCSV(int option){
		final String COMMA_DELIMITER = ",";
		final  String NEW_LINE_SEPARATOR = "\n";
		final  String HEADER = "Hobsons_CRM_ID,datecreated,firstname,surname,email,email2,dateofbirth,SubscriptionStatus_Email,SubscriptionStatus_Phone,SubscriptionStatus_SMS,International,Countryofresidence,CorrespondenceAddressPostcode,CorrespondenceAddressMobile,course,courseareaofstudy,BD,HQ,ContactChannel";
		String filename = null;
		if(option == 1){
			filename = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\DATALOAD\\Final_Dataload.csv";
		}
		else if(option == 2){
			filename = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\DATALOAD\\C1_OriginalContact.csv";
		}
		else{
			filename = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\DATALOAD\\C1_NewContact.csv";
		}
		
		FileWriter fileWriter = null;
		try{
			fileWriter = new FileWriter(filename);
			fileWriter.append(HEADER);
			fileWriter.append(NEW_LINE_SEPARATOR);
			for(Record rec : this.recordCollector){
				fileWriter.append(((rec.getHobsons_CRM_ID()!=null)? rec.getHobsons_CRM_ID(): ""));
				fileWriter.append(COMMA_DELIMITER);
				//DATE CREATED COMMA DELIMITER
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getFirstName()!=null)? rec.getFirstName(): ""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getLastName() !=null)? rec.getLastName(): ""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getEmail()!=null)? rec.getEmail(): ""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getEmail2()!=null)? rec.getEmail2(): ""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getDOB()!=null)? rec.getDOB(): ""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getSubscription_Email()!=null)? rec.getSubscription_Email():""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getSubscription_Phone()!=null)? rec.getSubscription_Phone(): ""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getSubscription_SMS()!=null)? rec.getSubscription_SMS(): ""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getInternational() !=null)? rec.getInternational():""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getCountryOfResidence()!=null)? rec.getCountryOfResidence():""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getCorrespondenceAddressPostcode()!=null)? rec.getCorrespondenceAddressPostcode():""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getCorrespondenceMobile()!=null)? rec.getCorrespondenceMobile():""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getCourse()!=null)? rec.getCourse():""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getCourseAreaOfStudy()!=null)? rec.getCourseAreaOfStudy():""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getBestDescribesYou()!=null)? rec.getBestDescribesYou(): ""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getHighestQualification()!=null)? rec.getHighestQualification():""));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(((rec.getContactChannel()!=null)? rec.getContactChannel(): ""));
				fileWriter.append(NEW_LINE_SEPARATOR);
			}
			System.out.println("CSV created successfully");
			
		}
		catch(Exception e){
			log.error("Error writing to CSV: " + e.getMessage());
		}
		finally{
			try{
				fileWriter.flush();
				fileWriter.close();
			}catch(Exception e){
				log.error("Error flushing / closing csv file: "+ e.getMessage());
			}
		}
	}
}
